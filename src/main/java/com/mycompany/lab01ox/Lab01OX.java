/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab01ox;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author kantinan
 */
public class Lab01OX {

static String[] board;
static String turn;


 static void board()
    {
        System.out.println("-------------");
        System.out.println("| " + board[0] + " | "
                           + board[1] + " | " + board[2]
                           + " |");
        System.out.println("-------------");
        System.out.println("| " + board[3] + " | "
                           + board[4] + " | " + board[5]
                           + " |");
        System.out.println("-------------");
        System.out.println("| " + board[6] + " | "
                           + board[7] + " | " + board[8]
                           + " |");
        System.out.println("-------------");
    }
 static String checkWin()
    {for (int a = 0; a < 8; a++) {
        String line = null;
 
        switch (a) {
        case 0:
            line = board[0] + board[1] + board[2];
            break;
        case 1:
            line = board[3] + board[4] + board[5];
            break;
            case 2:
            line = board[6] + board[7] + board[8];
            break;
        case 3:
            line = board[0] + board[3] + board[6];
            break;
        case 4:
            line = board[1] + board[4] + board[7];
            break;
        case 5:
            line = board[2] + board[5] + board[8];
            break;
        case 6:
            line = board[0] + board[4] + board[8];
            break;
            case 7:
            line = board[2] + board[4] + board[6];
            break;
        }
        
        if (line.equals("XXX")) {
            return "X";
        }
             
       
        else if (line.equals("OOO")) {
            return "O";
        }
    }
    for (int a = 0; a < 9; a++) {
            if (Arrays.asList(board).contains(
                    String.valueOf(a + 1))) {
                break;
            }
            else if (a == 8) {
                return "draw";
            }
        }
 
      
        System.out.println(
            turn + "'s turn; enter a slot number to place "
            + turn + " in:");
        return null;
    }
 
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        board = new String[9];
        turn = "X";
        String winner = null;
      
        for (int a = 0; a < 9; a++) {
            board[a] = String.valueOf(a + 1);
        }
        
         System.out.println("Tic-Tac-Toe!");
         System.out.println(
                "Current board layout:");
        board();
        
        System.out.println(
            "X Player x, enter an number to place your mark :");
        System.out.println(
                "Current board layout:");
        
        System.out.println(
            "X will play first. Enter a slot number to place X in:");
 
        while (winner == null) {
            int numInput;
        
        try {
                numInput = in.nextInt();
                if (!(numInput > 0 && numInput <= 9)) {
                    System.out.println(
                        "Invalid input; re-enter slot number:");
                    continue;
                }
            }
            catch (InputMismatchExceptiontchExceptionException e) {
                System.out.println(
                    "Invalid input; re-enter slot number:");
                continue;
            }
        
         if (board[numInput - 1].equals(
                    String.valueOf(numInput))) {
                board[numInput - 1] = turn;
 
                if (turn.equals("X")) {
                    turn = "O";
                }
                else {
                    turn = "X";
                }
 
                board();
                winner = checkWin();
            }
            else {
                System.out.println(
                    "Slot already taken; re-enter slot number:");
            }
        }
        if (winner.equalsIgnoreCase("draw")) {
            System.out.println(
                "It's a draw! Thanks for playing.");
        }
       
        // For winner -to display Congratulations! message.
        else {
            System.out.println(
                "Congratulations! " + winner
                + "'s have won! Thanks for playing.");
        }
      in.close();
    }
}
    